#include<iostream>
#include<cmath>
#include<fstream>
#include<vector>

#define PI 3.14159265

using namespace std;

int main()
{
	double radius=6.0;
	unsigned n,i;
	long double theta;
	
	cout<<"\nEnter number of boundary nodes: ";
	cin>>n;
	long double dt,avg_curvature,theta1,theta2,length1,length2,distancex1,distancex2,distancey1,distancey2;
	vector<double> x;
	vector<double> y;
	vector<double> curvature;
	
	dt=360/n;
	i=0;
	//Equation of circle: x^2 + y^2 = radius^2
	//Getting x and y coordinates
	
	for(theta=0;theta<=360;theta=theta+dt)
	{
		x.push_back(radius*cos (theta*PI/180));
		y.push_back(radius*sin (theta*PI/180));
		
		}
avg_curvature=0;
ofstream curvatureFile;

for(i=0;i<n;i++)
{
	curvatureFile.open("BoundaryIndices_36nodes_r_6.txt", ios::out | ios::app | ios::binary);
	curvatureFile<<"\n"<<x[i]<<"	"<<y[i];
	curvatureFile.close();
	}	
	
	unsigned j,k,l;
	
	//Implementing curvature
	for(i=0;i<n;i++)
	{
		j=i%n;
		k=(i+1)%n;
		l=(i+2)%n;
		
		//theta1 = atan2((y[k]-y[j]),(x[k]-x[j]));
		
		//theta2 = atan2((y[l]-y[k]),(x[l]-x[k]));
				
		distancex1 = (x[k] - x[j])*(x[k] - x[j]);
		distancey1 = (y[k] - y[j])*(y[k] - y[j]);
		distancex2 = (x[l] - x[k])*(x[l] - x[k]);
		distancey2 = (y[l] - y[k])*(y[l] - y[k]);
		length1 = sqrt(distancex1 + distancey1);
		length2 = sqrt(distancex2 + distancey2);
		
		
		theta = acos(((x[k] - x[j])*(x[l] - x[k])+(y[k] - y[j])*(y[l] - y[k]))/(length1*length2));
		
		curvature.push_back(2*(theta)/(length1+length2));
		
		//clog<<j<<" "<<k<<" "<<l<<" "<<theta1<<" "<<theta2<<" "<<length1<<" "<<length2<<"  "<<curvature[i]<<endl;
		cout<<"\n	"<<i<<"	theta	"<<theta<<"	length1	"<<length1<<"	length2	"<<length2<<"	curvature	"<<curvature[j];
		avg_curvature = avg_curvature + (curvature[j]*curvature[j]);
		
		}
		avg_curvature = avg_curvature/n;
		cout<<"\n Avergae Squared Curvature		"<<avg_curvature;
		avg_curvature = sqrt(avg_curvature);
		
		cout<<"\n Curvature		"<<avg_curvature<<endl;
	
	return 0;
		
	}
