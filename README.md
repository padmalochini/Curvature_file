The code for implementing curvature is written inside Curvature.cpp.

10 node and 36 node boundary nodes for the circle of radius 6 is given inside BoundaryIndices_10nodes_r_6.txt and BoundaryIndices_36nodes_r_6.txt

The output of the curvature is not as expected. 

For 10 noded boundary indices, the curvature gives the value 0.25416
For 36 noded boundary indices, the curvature gives the value 0.493633

